package org.gradle;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.ParseException;

import org.junit.Test;

import jolie.runtime.FaultException;
import jolie.runtime.Value;
import jolie.runtime.ValueVector;

/**
 * @author Margherita Donnici
 *
 */
public class CalendarConnectorTest {
	CalendarConnector cal = new CalendarConnector();

	@Test
	public void serviceAccountInitTest() throws FaultException {
		Value serviceAccountCred = createAuthValue();
		cal.serviceAccountInit(serviceAccountCred);
	}

	@Test
	public void addCalendarTest() throws IOException, GeneralSecurityException, FaultException {
		Value serviceAccountCred = createAuthValue();
		cal.serviceAccountInit(serviceAccountCred);
		Value val = Value.create();
		val.setValue("testcal");
		cal.addCalendar(val);
	}

	@Test
	public void addEventTest() throws FaultException {
		Value serviceAccountCred = createAuthValue();
		cal.serviceAccountInit(serviceAccountCred);

		Value val = Value.create();
		val.setValue("testcal");
		String calendarId = cal.addCalendar(val);

		// Create Event Value
		Value eventReq = Value.create();
		eventReq.getFirstChild("calendarId").setValue(calendarId);
		eventReq.getFirstChild("name").setValue("TestEvent");
		eventReq.getFirstChild("startDate").setValue("04.08.2015,1300");
		eventReq.getFirstChild("endDate").setValue("04.08.2015,1400");

		cal.addEvent(eventReq);
	}
	
	@Test
	public void deleteEventTest() throws FaultException {
		Value serviceAccountCred = createAuthValue();
		cal.serviceAccountInit(serviceAccountCred);

		Value val = Value.create();
		val.setValue("testcal");
		String calendarId = cal.addCalendar(val);

		// Create Event
		Value eventReq = Value.create();
		eventReq.getFirstChild("calendarId").setValue(calendarId);
		eventReq.getFirstChild("name").setValue("TestEvent");
		eventReq.getFirstChild("startDate").setValue("04.08.2015,1300");
		eventReq.getFirstChild("endDate").setValue("04.08.2015,1400");

		String eventId = cal.addEvent(eventReq);
		
		// Delete Event
		Value eventDel = Value.create();
		eventDel.getFirstChild("calendarId").setValue(calendarId);
		eventDel.getFirstChild("eventId").setValue(eventId);
		cal.deleteEvent(eventDel);	
	}
	
	@Test
	public void deleteCalendarTest() throws FaultException {
		Value serviceAccountCred = createAuthValue();
		cal.serviceAccountInit(serviceAccountCred);

		Value val = Value.create();
		val.setValue("testcal");
		String calendarId = cal.addCalendar(val);
		Value calendarVal = Value.create();
		calendarVal.setValue(calendarId);	
		cal.deleteCalendar(calendarVal);	
	}
	
	@Test
	public void getEventStatusTest() throws FaultException {
		Value serviceAccountCred = createAuthValue();
		cal.serviceAccountInit(serviceAccountCred);

		Value val = Value.create();
		val.setValue("testcal");
		String calendarId = cal.addCalendar(val);

		// Create Event
		Value eventReq = Value.create();
		eventReq.getFirstChild("calendarId").setValue(calendarId);
		eventReq.getFirstChild("name").setValue("TestEvent");
		eventReq.getFirstChild("startDate").setValue("04.08.2015,1300");
		eventReq.getFirstChild("endDate").setValue("04.08.2015,1400");

		String eventId = cal.addEvent(eventReq);
		
		Value event = Value.create();
		event.getFirstChild("calendarId").setValue(calendarId);
		event.getFirstChild("eventId").setValue(eventId);
		
		assertTrue(("tentative".equals(cal.getEventStatus(event))));
		cal.confirmEvent(event);
		assertTrue(("confirmed".equals(cal.getEventStatus(event))));
		cal.cancelEvent(event);
		assertTrue(("cancelled".equals(cal.getEventStatus(event))));
	}
	
	@Test
	public void confirmEventTest() throws FaultException {
		Value serviceAccountCred = createAuthValue();
		cal.serviceAccountInit(serviceAccountCred);

		Value val = Value.create();
		val.setValue("testcal");
		String calendarId = cal.addCalendar(val);

		// Create Event
		Value eventReq = Value.create();
		eventReq.getFirstChild("calendarId").setValue(calendarId);
		eventReq.getFirstChild("name").setValue("TestEvent");
		eventReq.getFirstChild("startDate").setValue("04.08.2015,1300");
		eventReq.getFirstChild("endDate").setValue("04.08.2015,1400");

		String eventId = cal.addEvent(eventReq);
		
		// Confirm Event
		Value eventConf = Value.create();
		eventConf.getFirstChild("calendarId").setValue(calendarId);
		eventConf.getFirstChild("eventId").setValue(eventId);
		cal.confirmEvent(eventConf);
		assertTrue(("confirmed".equals(cal.getEventStatus(eventConf))));
	}
	
	@Test
	public void cancelEventTest() throws FaultException {
		Value serviceAccountCred = createAuthValue();
		cal.serviceAccountInit(serviceAccountCred);

		Value val = Value.create();
		val.setValue("testcal");
		String calendarId = cal.addCalendar(val);

		// Create Event
		Value eventReq = Value.create();
		eventReq.getFirstChild("calendarId").setValue(calendarId);
		eventReq.getFirstChild("name").setValue("TestEvent");
		eventReq.getFirstChild("startDate").setValue("04.08.2015,1300");
		eventReq.getFirstChild("endDate").setValue("04.08.2015,1400");

		String eventId = cal.addEvent(eventReq);
		
		// Confirm Event
		Value eventCanc = Value.create();
		eventCanc.getFirstChild("calendarId").setValue(calendarId);
		eventCanc.getFirstChild("eventId").setValue(eventId);
		cal.cancelEvent(eventCanc);
		assertTrue(("cancelled".equals(cal.getEventStatus(eventCanc))));
	}

	@Test
	public void isBusyTest() throws IOException, GeneralSecurityException, ParseException, FaultException {
		Value serviceAccountCred = createAuthValue();
		cal.serviceAccountInit(serviceAccountCred);

		Value val = Value.create();
		val.setValue("testcal");
		String calendarId = cal.addCalendar(val);

		Value eventReq = createEventValue(calendarId, "Test1", "16.08.2015,1400", "16.08.2015,1500");

		Value busyReq1 = Value.create();
		busyReq1.getFirstChild("calendarId").setValue(calendarId);
		busyReq1.getFirstChild("startDate").setValue("16.08.2015,1200");
		busyReq1.getFirstChild("endDate").setValue("16.08.2015,1800");
		Value busyReq2 = Value.create();
		busyReq2.getFirstChild("calendarId").setValue(calendarId);
		busyReq2.getFirstChild("startDate").setValue("26.09.2015,1500");
		busyReq2.getFirstChild("endDate").setValue("26.09.2015,1800");

		cal.addEvent(eventReq);

		assertTrue(cal.isBusy(busyReq1));
		assertFalse(cal.isBusy(busyReq2));
	}

	@Test
	public void getFreeCalendarsTest() throws IOException, GeneralSecurityException, ParseException, FaultException {
		Value serviceAccountCred = createAuthValue();
		cal.serviceAccountInit(serviceAccountCred);

		Value val1 = Value.create();
		val1.setValue("testcal5");
		String cal1 = cal.addCalendar(val1);
		Value val2 = Value.create();
		val2.setValue("testcal6");
		String cal2 = cal.addCalendar(val2);

		Value eventReq = createEventValue(cal1, "Test1", "16.08.2015,1400", "16.08.2015,1500");

		Value busyReq = Value.create();
		busyReq = createCalendarsValue(cal1, cal2, "16.08.2015,1400", "16.08.2015,1500");

		cal.addEvent(eventReq);

		cal.getFreeCalendars(busyReq);
	}

	@Test
	public void getEventsTest() throws IOException, GeneralSecurityException, ParseException, FaultException {
		Value serviceAccountCred = createAuthValue();
		cal.serviceAccountInit(serviceAccountCred);

		Value val = Value.create();
		val.setValue("testcal");
		String calendarId = cal.addCalendar(val);

		Value eventReq = createEventValue(calendarId, "Test1", "16.08.2015,1400", "16.08.2015,1500");
		cal.addEvent(eventReq);

		Value calVal = Value.create();
		calVal.setValue(calendarId);
		cal.getEvents(calVal);

	}
	
	@Test
	public void getTentativeEventsTest() throws IOException, GeneralSecurityException, ParseException, FaultException {
		Value serviceAccountCred = createAuthValue();
		cal.serviceAccountInit(serviceAccountCred);

		Value val = Value.create();
		val.setValue("testcal");
		String calendarId = cal.addCalendar(val);

		Value eventReq = createEventValue(calendarId, "Test1", "16.08.2015,1400", "16.08.2015,1500");
		cal.addEvent(eventReq);

		Value calVal = Value.create();
		calVal.setValue(calendarId);
		cal.getTentativeEvents(calVal);

	}

	// Util methods for creating Value objects

	private Value createAuthValue() {
		String emailStr = "614373334838-qs8pabv40obje534pk74i519rrp8f7c6@developer.gserviceaccount.com";
		String keyStr = "servizioPrenotazioni-32b0fb00864b.p12";
		String appNameStr = "servizio-prenotazioni/1.0";

		Value serviceAccountCred = Value.create();
		serviceAccountCred.getFirstChild("email").setValue(emailStr);
		serviceAccountCred.getFirstChild("key").setValue(keyStr);
		serviceAccountCred.getFirstChild("appName").setValue(appNameStr);

		return serviceAccountCred;
	}

	private Value createEventValue(String calendarId, String eventname, String startDate, String endDate) {
		Value eventReq = Value.create();
		eventReq.getFirstChild("calendarId").setValue(calendarId);
		eventReq.getFirstChild("name").setValue(eventname);
		eventReq.getFirstChild("startDate").setValue(startDate);
		eventReq.getFirstChild("endDate").setValue(endDate);
		return eventReq;
	}

	private Value createCalendarsValue(String calendarId1, String calendarId2, String startDate, String endDate) {
		Value busyReq = Value.create();
		ValueVector calendarIds = ValueVector.create();
		calendarIds = busyReq.getChildren("calendars");
		calendarIds.get(0).setValue(calendarId1);
		calendarIds.get(1).setValue(calendarId2);
		busyReq.getFirstChild("startDate").setValue(startDate);
		busyReq.getFirstChild("endDate").setValue(endDate);
		return busyReq;
	}
}
