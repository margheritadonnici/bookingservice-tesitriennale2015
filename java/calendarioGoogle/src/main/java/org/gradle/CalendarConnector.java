package org.gradle;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

// Google service account imports
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar.Freebusy;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;
import com.google.api.services.calendar.model.FreeBusyCalendar;
import com.google.api.services.calendar.model.FreeBusyRequest;
import com.google.api.services.calendar.model.FreeBusyRequestItem;
import com.google.api.services.calendar.model.FreeBusyResponse;

import jolie.runtime.FaultException;
import jolie.runtime.JavaService;
import jolie.runtime.Value;
import jolie.runtime.ValueVector;
import jolie.runtime.embedding.RequestResponse;

public class CalendarConnector extends JavaService {
	
	/**
	 * @author Margherita Donnici
	 *
	 */

	private static com.google.api.services.calendar.Calendar client;

	/**
	 * Authenticate with service account credentials and initializes global calendar instance
	 * 
	 * @param serviceAccountCred Jolie Value containing service account email, key and application name
	 * @throws IOException
	 * @throws GeneralSecurityException
	 * @throws FaultException
	 */

	@RequestResponse
	public void serviceAccountInit(Value serviceAccountCred) throws FaultException {

		// Retrieve parameters from Jolie Value tree
		String emailCredential = serviceAccountCred.getFirstChild("email").strValue();
		String key = serviceAccountCred.getFirstChild("key").strValue();
		String applicationName = serviceAccountCred.getFirstChild("appName").strValue();
		try {
			// Create GoogleCredential object with service account credential
			JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
			HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
			GoogleCredential credential = new GoogleCredential.Builder().setTransport(httpTransport)
					.setJsonFactory(JSON_FACTORY).setServiceAccountId(emailCredential)
					.setServiceAccountPrivateKeyFromP12File(new File(key))
					.setServiceAccountScopes(Collections.singleton(CalendarScopes.CALENDAR)).build();

			// Set up global calendar instance
			client = new com.google.api.services.calendar.Calendar.Builder(httpTransport, JSON_FACTORY, credential)
					.setApplicationName(applicationName).build();

		}
		catch (IOException e) {
			throw new FaultException("IoException", e);
		}
		catch (GeneralSecurityException e) {
			throw new FaultException("GeneralSecurityException", e);
		}
	}

	// Methods for handling calendars

	/**
	 * Create a calendar
	 * 
	 * @param name Calendar Name
	 * @return calendar id
	 * @throws IOException
	 */
	@RequestResponse
	public String addCalendar(Value nameVal) throws FaultException {
		try {
			String name = nameVal.strValue();
			Calendar entry = new Calendar();
			entry.setSummary(name);
			Calendar result;
			result = client.calendars().insert(entry).execute();
			return result.getId();
		}
		catch (IOException e) {
			throw new FaultException("IOException", e);
		}

	}

	/**
	 * Deletes a calendar
	 * 
	 * @param calendarId id of the calendar to delete
	 * @throws IOException
	 */
	@RequestResponse
	public void deleteCalendar(Value calendarVal) throws FaultException {
		try {
			String calendarId = calendarVal.strValue();
			client.calendars().delete(calendarId).execute();
		}
		catch (IOException e) {
			throw new FaultException("IOException", e);

		}
	}

	// Methods for handling events

	/**
	 * Add an event to a calendar
	 * 
	 * @param eventReq Jolie Value containing calendar id, name of event, start date and end date ( format: dd.MM.yyyy,HHmm)
	 * @return id of the created event
	 * @throws IOException
	 * @throws ParseException
	 */
	@RequestResponse
	public String addEvent(Value eventReq) throws FaultException {
		try {
			// Retrieve parameters from Jolie Value tree
			String calendarId = eventReq.getFirstChild("calendarId").strValue();
			String name = eventReq.getFirstChild("name").strValue();
			String startDateStr = eventReq.getFirstChild("startDate").strValue();
			String endDateStr = eventReq.getFirstChild("endDate").strValue();

			Event event = new Event();
			event.setSummary(name);

			DateTime start = convertToDateTime(startDateStr);
			event.setStart(new EventDateTime().setDateTime(start));
			DateTime end = convertToDateTime(endDateStr);
			event.setEnd(new EventDateTime().setDateTime(end));

			event.setStatus("tentative");
			Event result = client.events().insert(calendarId, event).execute();
			return result.getId();
		}
		catch (IOException e) {
			throw new FaultException("IOException", e);

		}
		catch (ParseException e) {
			throw new FaultException("ParseException", e);

		}
	}

	/**
	 * Sets an event's status to "confirmed"
	 * 
	 * @param calendarId id of the calendar the event belongs to
	 * @param eventId id of the event to be deleted
	 * @throws IOException
	 */
	@RequestResponse
	public void confirmEvent(Value ids) throws FaultException {
		try {
			String calendarId = ids.getFirstChild("calendarId").strValue();
			String eventId = ids.getFirstChild("eventId").strValue();
			Event ev = client.events().get(calendarId, eventId).execute();
			ev.setStatus("confirmed");
			client.events().update(calendarId, eventId, ev).execute();
		}
		catch (IOException e) {
			throw new FaultException("IOException", e);
		}
	}

	/**
	 * Sets an event's status to "cancelled"
	 * 
	 * @param calendarId id of the calendar the event belongs to
	 * @param eventId id of the event to be deleted
	 * @throws IOException
	 */
	@RequestResponse
	public void cancelEvent(Value ids) throws FaultException {
		String calendarId = ids.getFirstChild("calendarId").strValue();
		String eventId = ids.getFirstChild("eventId").strValue();
		Event ev;
		try {
			ev = client.events().get(calendarId, eventId).execute();

			ev.setStatus("cancelled");
			client.events().update(calendarId, eventId, ev).execute();
		}
		catch (IOException e) {
			throw new FaultException("IOException", e);
		}
	}

	/**
	 * Checks an event's status
	 * 
	 * @param calendarId id of the calendar the event belongs to
	 * @param eventId id of the event to be deleted
	 * @return
	 * @throws IOException
	 */
	@RequestResponse
	public String getEventStatus(Value ids) throws FaultException {
		try {
			String calendarId = ids.getFirstChild("calendarId").strValue();
			String eventId = ids.getFirstChild("eventId").strValue();
			Event ev = client.events().get(calendarId, eventId).execute();
			String status = ev.getStatus();
			return status;
		}
		catch (IOException e) {
			throw new FaultException("IOException", e);

		}
	}

	/**
	 * Delete an event from a calendar
	 * 
	 * @param calendarId id of the calendar the event belongs to
	 * @param eventId id of the event to be deleted
	 * @throws IOException
	 */
	@RequestResponse
	public void deleteEvent(Value ids) throws FaultException {
		String calendarId = ids.getFirstChild("calendarId").strValue();
		String eventId = ids.getFirstChild("eventId").strValue();
		try {
			client.events().delete(calendarId, eventId).execute();
		}
		catch (IOException e) {
			throw new FaultException("IOException", e);
		}
	}

	/**
	 * List all the events of a calendar with current date time as lower bound for end time
	 * 
	 * @param calendarId id of the calendar
	 * @return Jolie value with an array containing all of the events
	 * @throws IOException
	 */
	@RequestResponse
	public Value getEvents(Value calValue) throws FaultException {
		try {
			String calendarId = calValue.strValue();

			Value response = Value.create();
			ValueVector eventValues = response.getChildren("event");

			// Get current time and date
			Date date = new Date();
			DateTime todayDate = new DateTime(date, TimeZone.getTimeZone("GMT+02"));

			// Iterate over the events in the specified calendar
			String pageToken = null;
			int index = 0;
			do {
				Events eventList = client.events().list(calendarId).setTimeMin(todayDate).setPageToken(pageToken)
						.execute();
				List<Event> items = eventList.getItems();
				for (Event event : items) {
					// Insert events in Value to return
					Value eventVal = eventValues.get(index++);
					eventVal.getFirstChild("calendarId").setValue(calendarId);
					eventVal.getFirstChild("name").setValue(event.getSummary());
					eventVal.getFirstChild("eventId").setValue(event.getId());
					String start = event.getStart().getDateTime().toStringRfc3339();
					String end = event.getEnd().getDateTime().toStringRfc3339();
					eventVal.getFirstChild("startDate").setValue(start);
					eventVal.getFirstChild("endDate").setValue(end);

				}
				pageToken = eventList.getNextPageToken();
			} while (pageToken != null);
			return response;
		}

		catch (IOException e) {
			throw new FaultException("IOException", e);
		}
	}

	/**
	 * List all the events of a calendar with current date time as lower bound for end time
	 * 
	 * @param calValue value containing id of the calendar
	 * @return Jolie value with an array containing all of the events
	 * @throws IOException
	 */
	@RequestResponse
	public Value getTentativeEvents(Value calValue) throws FaultException {
		try {
			String calendarId = calValue.strValue();

			Value response = Value.create();
			ValueVector eventValues = response.getChildren("event");

			// Get current time and date
			Date date = new Date();
			DateTime todayDate = new DateTime(date, TimeZone.getTimeZone("GMT+02"));

			// Iterate over the events in the specified calendar
			String pageToken = null;
			int index = 0;
			do {
				Events eventList = client.events().list(calendarId).setTimeMin(todayDate).setPageToken(pageToken)
						.execute();
				List<Event> items = eventList.getItems();
				for (Event event : items) {
					// Insert events in Value to return if pending confirmation
					if (event.getStatus().equals("tentative")) {
						Value eventVal = eventValues.get(index++);
						eventVal.getFirstChild("calendarId").setValue(calendarId);
						eventVal.getFirstChild("name").setValue(event.getSummary());
						eventVal.getFirstChild("eventId").setValue(event.getId());
						String start = event.getStart().getDateTime().toStringRfc3339();
						String end = event.getEnd().getDateTime().toStringRfc3339();
						eventVal.getFirstChild("startDate").setValue(start);
						eventVal.getFirstChild("endDate").setValue(end);
					}
				}
				pageToken = eventList.getNextPageToken();
			} while (pageToken != null);
			return response;
		}

		catch (IOException e) {
			throw new FaultException("IOException", e);

		}
	}

	// Methods to retrieve free/busy information

	/**
	 * Checks if a calendar results as busy during given timespan ( i.e. if there are any events during that timespan)
	 * 
	 * @param BusyReq Jolie Value containing calendar Id, start and end of timespan ( "dd.MM.yyyy,HHmm" format)
	 * @return true if there is an event during the given timespan on that calendar, otherwise false
	 * @throws ParseException
	 * @throws IOException
	 */
	@RequestResponse
	public Boolean isBusy(Value BusyReq) throws FaultException {
		try {
			// Retrieve parameters from Jolie Value
			String calendarId = BusyReq.getFirstChild("calendarId").strValue();
			String startDateStr = BusyReq.getFirstChild("startDate").strValue();
			String endDateStr = BusyReq.getFirstChild("endDate").strValue();

			// Create & set the FreeBusyRequestItem to the given calendar
			FreeBusyRequestItem cal = new FreeBusyRequestItem().setId(calendarId);
			List<FreeBusyRequestItem> calList = new ArrayList<FreeBusyRequestItem>();
			calList.add(cal);
			// Create a FreeBusyRequest object for the query
			FreeBusyRequest req = new FreeBusyRequest();
			req.setItems(calList);
			DateTime startDate = convertToDateTime(startDateStr);
			DateTime endDate = convertToDateTime(endDateStr);
			req.setTimeMin(startDate);
			req.setTimeMax(endDate);

			FreeBusyResponse response;
			// Query
			Freebusy.Query query = client.freebusy().query(req);
			query.setFields("calendars"); // partial GET to retrieve only needed fields
			response = query.execute();

			for (Map.Entry<String, FreeBusyCalendar> busyCalendar : response.getCalendars().entrySet()) {
				if (busyCalendar.getValue().getBusy().isEmpty()) {
					return false;
				}
			}
			return true;
		}
		catch (IOException e) {
			throw new FaultException("IOException", e);
		}
		catch (ParseException e) {
			throw new FaultException("ParseException", e);
		}
	}

	/**
	 * Returns all free calendars during given timespan ( i.e. if there are no events during that timespan)
	 * 
	 * @param BusyReq Jolie Value containing calendar Id, start and end of timespan ( "dd.MM.yyyy,HHmm" format)
	 * @return true if there is an event during the given timespan on that calendar, otherwise false
	 * @throws ParseException
	 * @throws IOException
	 */
	@RequestResponse
	public Value getFreeCalendars(Value BusyReq) throws FaultException {
		try {
			// Retrieve parameters from Jolie Value
			ValueVector calendarIds = ValueVector.create();
			calendarIds = BusyReq.getChildren("calendars");
			String startDateStr = BusyReq.getFirstChild("startDate").strValue();
			String endDateStr = BusyReq.getFirstChild("endDate").strValue();

			// Create an array of FreeBusyRequestItem with the given calendar ids
			List<FreeBusyRequestItem> calList = new ArrayList<FreeBusyRequestItem>();
			for (int i = 0; i < calendarIds.size(); i++) {
				String calId = calendarIds.get(i).strValue();
				FreeBusyRequestItem cal = new FreeBusyRequestItem().setId(calId);
				calList.add(cal);
			}
			// Create a FreeBusyRequest object for the query
			FreeBusyRequest req = new FreeBusyRequest();
			req.setItems(calList);
			DateTime startDate = convertToDateTime(startDateStr);
			DateTime endDate = convertToDateTime(endDateStr);
			req.setTimeMin(startDate);
			req.setTimeMax(endDate);

			FreeBusyResponse response;
			// Query
			Freebusy.Query query = client.freebusy().query(req);
			query.setFields("calendars"); // partial GET to retrieve only needed fields
			response = query.execute();

			Value ret = Value.create();
			ValueVector idValues = ret.getChildren("calendarIds");
			int index = 0;
			for (Map.Entry<String, FreeBusyCalendar> busyCalendar : response.getCalendars().entrySet()) {
				if (busyCalendar.getValue().getBusy().isEmpty()) {
					idValues.get(index++).setValue(busyCalendar.getKey());
				}
			}
			return ret;
		}
		catch (IOException e) {
			throw new FaultException("IOException", e);
		}
		catch (ParseException e) {
			throw new FaultException("ParseException", e);

		}
	}

	/**
	 * Converts a string date in dd.MM.yyyy,HHmm format as a DateTime object
	 * 
	 * @param dateStr
	 * @return DateTime corresponding date as DateTime object
	 * @throws ParseException
	 */
	private DateTime convertToDateTime(String dateStr) throws ParseException {
		DateFormat format = new SimpleDateFormat("dd.MM.yyyy,HHmm");
		Date date = format.parse(dateStr);
		DateTime convertedDate = new DateTime(date, TimeZone.getTimeZone("GMT+02"));
		return convertedDate;
	}

}