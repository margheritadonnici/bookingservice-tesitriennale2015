-- Adminer 4.2.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `prenotazione`;
CREATE TABLE `prenotazione` (
  `idprenotazione` int(11) NOT NULL AUTO_INCREMENT,
  `idcalendario` varchar(150) NOT NULL,
  `idevento` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `owner` varchar(150) NOT NULL,
  PRIMARY KEY (`idprenotazione`),
  KEY `idcalendario` (`idcalendario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `spazio`;
CREATE TABLE `spazio` (
  `proprietario` varchar(50) NOT NULL,
  `id` varchar(150) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `descrizione` varchar(200) DEFAULT NULL,
  `indirizzo` varchar(100) DEFAULT NULL,
  `quartiere` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `proprietario` (`proprietario`),
  CONSTRAINT `spazio_ibfk_1` FOREIGN KEY (`proprietario`) REFERENCES `utente` (`username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `utente`;
CREATE TABLE `utente` (
  `username` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2015-08-24 04:43:08
