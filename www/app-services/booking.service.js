(function () {
    'use strict';

    angular
        .module('app')
        .factory('BookingService', BookingService);

    BookingService.$inject = ['$filter', '$http'];



    function BookingService($filter, $http) {
        var service = {};

        // Interface

        service.CancelBooking = CancelBooking;
        service.GetAllLocationsWithUser = GetAllLocationsWithUser;
        service.CreateLocation = CreateLocation;
        service.GetAllAvailableLocations = GetAllAvailableLocations;
        service.CreateBooking = CreateBooking;
        service.GetUpcomingBookings = GetUpcomingBookings;
        service.GetLocationDetails = GetLocationDetails;

        return service;
        
        // Private Functions

        function formatDateString(date) {
            return $filter('date')(date, "dd.MM.yyyy,HHmm");
        }

       // Parses a xml list of locations.
       // xmlString: An xml service response containing a list of locations 
       // Return: A javascript array of location objects.
        function parseLocationsXmlString(xmlString) {
            if (xmlString == null || xmlString === "") throw new TypeError("xmlString must not be null or empty.");
            var locations = {};
            $(xmlString).find("location").each(function () {
                var location = {
                    name: $(this).find("name").text(),
                    id: $(this).find("id").text(),
                }
                Array.push(locations, location);
            });
            return locations;
        }
        
        
        function handleError(error) {
            return function () {
                console.error(error);
                return { isError: true, errorMessage: error };
            };
        }
        
        // Public Functions

        function CancelBooking(bookingID, callback) {
            if (bookingID == null) throw new TypeError("bookingID must not be null.");
            var deleteBookingRequest = { bookingID: bookingID };
            $http.post('/deleteBooking', deleteBookingRequest)
                .then(function (serviceResponse) {
                    callback(serviceResponse.data);
                }, handleError("Error canceling booking."));
        }

        function GetAllAvailableLocations(startDate, endDate, callback) {
            if (!(startDate instanceof Date)) throw new TypeError("startDate must be a date.");
            if (!(endDate instanceof Date)) throw new TypeError("endDate must be a date.");
            var request = {
                startDate: formatDateString(startDate),
                endDate: formatDateString(endDate),
            };
            $http.post('/getAllAvailableLocations', request)
                .then(function (serviceResponse) {
                    var locations = serviceResponse.data.location;
                    callback(locations);
                }, handleError("Error getting available locations."));
        }

        function GetAllLocationsWithUser(callback) {
            $http.post('/getAllLocationsWithUser')
                .then(function (serviceResponse) {
                    if (serviceResponse.isError) {
                        callback({ isError: true, errorMessage: serviceResponse.errorMessage });
                    } else {
                        var locations = parseLocationsXmlString(serviceResponse.data);
                        callback(locations);
                    }
                }, handleError("Error getting all locations with user."));
        }

        function CreateLocation(locationRequest, callback) {
            if (locationRequest == null) throw new TypeError("locationRequest must not be null.");
            $http.post('/addNewLocation', locationRequest)
                .then(function (serviceResponse) {
                    callback(serviceResponse.data);
                }, handleError("Error creating location."));
        }


        function CreateBooking(calendarId, startDate, endDate, email, callback) {
            if (calendarId == null) throw new TypeError("calendarId must not be null.");
            if (email == null || email === "") throw new TypeError("email must not be null or empty.");
            var createBookingRequest = {
                calendarId: calendarId,
                startDate: formatDateString(startDate),
                endDate: formatDateString(endDate),
                email: email,
            };
            $http.post('/createBooking', createBookingRequest)
                .then(function (serviceResponse) {
                    callback(serviceResponse.data);
                }, handleError("Error creating booking."));
        }

        function GetUpcomingBookings(locationId, callback) {
            if (locationId == null) throw new TypeError("locationId must not be null.");
            $http.post('/getUpcomingBookings', { locationId: locationId })
                .then(function (serviceResponse) {
                    callback(serviceResponse.data);
                }, handleError("Error getting upcoming bookings."));
        }

        function GetLocationDetails(locationId, callback) {
            if (locationId == null) throw new TypeError("locationId must not be null.");
            $http.post('/getLocationDetails', { locationId: locationId })
                .then(function (serviceResponse) {
                    callback(serviceResponse.data);
                }, handleError("Error getting location details."));
        }

    };
})();
