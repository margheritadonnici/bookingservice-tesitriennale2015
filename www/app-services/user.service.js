(function () {
    'use strict';

    angular
        .module('app')
        .factory('UserService', UserService);

    UserService.$inject = ['$http'];
    function UserService($http) {
        var service = {};

        service.Create = Create;

        return service;

        function Create(user) {
            return $http.post('/createUser', user)
                .then(handleSuccess, handleError('Error creating user'));
        }
        
        // private functions
 
        function handleSuccess(data) {
            if (data.isError) {
                return { isError: true, errorMessage: data.errorMessage };
            } else {
                return { isError: false };
            }

        }

        function handleError(error) {
            return function () {
                return { isError: true, errorMessage: error };
            };
        }
    }

})();