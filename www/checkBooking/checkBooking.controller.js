(function () {
    'use strict';

    angular
        .module('app')
        .controller('CheckBookingController', CheckBookingController);

    CheckBookingController.$inject = ['$location', 'BookingService'];
    function CheckBookingController($location, BookingService) {
        var vm = this;
        vm.cancelBooking = cancelBooking;
        vm.showResponse = false;
        vm.error = false;
        vm.dataLoading = false;

        function cancelBooking() {
            vm.dataLoading = true;
            
            BookingService.CancelBooking(vm.bookingId, function (response) {         
                if (response.isError) {
                    vm.dataLoading = false;
                    vm.error = true;
                    vm.errorMessage = response.errorMessage;
                } else {
                    vm.dataLoading = false;
                    vm.status = response;
                    vm.showResponse = true;
                }
            });
        };
    }

})();
