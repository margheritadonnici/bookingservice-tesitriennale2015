(function () {
    'use strict';

    angular
        .module('app')
        .controller('BookingController', BookingController);

    BookingController.$inject = ['$mdDialog', '$filter', '$location', 'BookingService'];
    function BookingController($mdDialog, $filter, $location, BookingService) {
        var vm = this;
        vm.findLocations = findLocations;
        vm.errorMessage = null;
        vm.showLocations = false;

        vm.showLocationDetails = showLocationDetails;
        vm.submitLocation = submitLocation;

        vm.timeChoices = {
            hours: range(1, 12),
            minutes: ["00", "15", "30", "45"],
            amOrPm: ["AM", "PM"],
        };

        vm.time = {
            start: {},
            end: {},
        };

        vm.user = {};

        function range(start, end) {
            var ret = [];
            for (var i = start; i <= end; i++) {
                ret.push(i);
            }
            return ret;
        };


        function formTimeToDate(formTime) {
            var date = new Date(vm.date.getTime()); // Copy the calendar picker date.
            date = new Date(date.getTime() + formTime.hours * 60 * 60 * 1000);
            date = new Date(date.getTime() + formTime.minutes * 60 * 1000);
            if (formTime.amOrPm === "pm") {
                date = new Date(date.getTime() + 12 * 60 * 60 * 1000); // Add 12 hours in milliseconds.
            }
            return date;
        };

        function reportBookingSuccess(bookingId) {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .clickOutsideToClose(true)
                    .title('Booking confirmation')
                    .content('Your booking has been confirmed, booking ID: ' + bookingId + '. You will receive an email notification shortly.')
                    .ariaLabel('Booking Confirmed')
                    .ok('Close')
                );
        };

        function showLocationDetails(location) {
            $mdDialog.show({
                controller: ['$scope', 'location', '$mdDialog', function ($scope, location, $mdDialog) {
                    $scope.location = null;
                    $scope.error = null;
                    $scope.errorMessage = null;
                    $scope.close = function () {
                        $mdDialog.hide();
                    };
                    // $scope.dataLoading = true;
                    (function initController() {
                        BookingService.GetLocationDetails(location.id, function (response) {
                            if (response.isError) {
                                $scope.error = true;
                                $scope.errorMessage = response.errorMessage;
                                $scope.dataLoading = false;
                            } else {
                                $scope.dataLoading = false;
                                $scope.location = response.location;
                            }
                        });
                    })();
                }],
                templateUrl: 'locationDetails/locationDetails.tmpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: {
                    location: location,
                },
            });
        };


        function findLocations() {

            vm.error = false;
            vm.showLocations = false;

            if (vm.date == null) {
                vm.errorMessage = "Error: A date must be specified.";
                vm.error = true;
                return;
            }

            var startDate = formTimeToDate(vm.time.start);
            var endDate = formTimeToDate(vm.time.end);


            if (startDate < Date.now()) {
                vm.errorMessage = "Error: Booking date must be in the future.";
                vm.error = true;
                return;
            }


            if (+startDate >= +endDate) {
                vm.errorMessage = "Error: End time must be after start time.";
                vm.error = true;
                return;
            }



            vm.dataLoading = true;
            BookingService.GetAllAvailableLocations(startDate, endDate,
                function (result) {
                    if (result.isError) {
                        vm.error = true;
                        vm.errorMessage = result.errorMessage;
                        vm.dataLoading = false;
                    } else if (result.noResultsFound) {
                        vm.error = true;
                        vm.errorMessage = "There are no available locations for that date and time.";
                        vm.dataLoading = false;
                    } else {
                        var locations = result;
                        vm.dataLoading = false;
                        vm.locations = locations;
                        vm.showLocations = true;
                    }
                });
        };


        function submitLocation() {
            var startDate = formTimeToDate(vm.time.start);
            var endDate = formTimeToDate(vm.time.end);
            var email = vm.user.email;
            vm.error = false;
            vm.dataLoading = true;
            console.info("Location submitted");
            
            BookingService.CreateBooking(vm.selectedLocationID, startDate, endDate, email, function (response) {
                if (response.isError) {
                    vm.error = true;
                    vm.errorMessage = response.errorMessage;
                    vm.dataLoading = false;
                } else {
                    vm.dataLoading = false;
                    reportBookingSuccess(response.bookingId);
                }

            });
        }


    }

})();
