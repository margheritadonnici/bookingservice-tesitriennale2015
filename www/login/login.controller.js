(function () {
    'use strict';

    angular
        .module('app')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', 'AuthenticationService' ];
    function LoginController($location, AuthenticationService) {
        var vm = this;

        vm.login = login;
        vm.error = false;

        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        })();

        function login() {
            vm.dataLoading = true;
            vm.error = false;
            AuthenticationService.Login(vm.username, vm.password, function (response) {
                if (response.success) {
                    AuthenticationService.SetCredentials(vm.username, vm.password);
                    $location.path('/userhome');
                } else {
                    vm.error = true;
                    vm.errorMessage = response.errorMessage;
                    vm.dataLoading = false;
                }
            });
        };
    }

})();
