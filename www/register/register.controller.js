(function () {
    'use strict';

    angular
        .module('app')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['UserService', '$location', '$rootScope'];
    function RegisterController(UserService, $location, $rootScope) {
        var vm = this;
        
        vm.registerError = false;
        vm.register = register;
        vm.dataLoading = false;

        function register() {
            vm.dataLoading = true;
            vm.registerError = false;
            UserService.Create(vm.user)
                .then(function (response) {
                    if (!response.isError) {
                        $location.path('/login');
                    } else {
                        vm.registerError = true;
                        vm.dataLoading = false;
                    }
                });
        }
    }

})();
