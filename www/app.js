(function () {
    'use strict';

    angular
        .module('app', ['ngMaterial','ngRoute', 'ngCookies', 'ngMessages'])
        .config(config)
        .run(run);

    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                controller: 'HomeController',
                templateUrl: 'home/home.view.html',
                controllerAs: 'vm'
            })

            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'login/login.view.html',
                controllerAs: 'vm'
            })

            .when('/register', {
                controller: 'RegisterController',
                templateUrl: 'register/register.view.html',
                controllerAs: 'vm'
            })
            
            .when('/bookingStatus', {
                controller: 'CheckBookingController',
                templateUrl: 'checkBooking/checkBooking.view.html',
                controllerAs: 'vm'
            })
            
            .when('/booking', {
                controller: 'BookingController',
                templateUrl: 'booking/booking.view.html',
                controllerAs: 'vm'
            })
            .when('/userhome', {
                controller: 'UserHomeController',
                templateUrl: 'userhome/userhome.view.html',
                controllerAs: 'vm'
            })
            .when('/addlocation', {
                controller: 'AddLocationController',
                templateUrl: 'addlocation/addlocation.view.html',
                controllerAs: 'vm'
            })
            .when('/location/:id', {
                controller: 'LocationController',
                templateUrl: 'location/location.view.html',
                controllerAs: 'vm'
            })


            .otherwise({ redirectTo: 'home/home.view.html' });
    }

    run.$inject = ['$rootScope', '$location', '$cookieStore', '$http'];
    function run($rootScope, $location, $cookieStore, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/login', '/register', '/bookingStatus','/booking','/addlocation','/']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && !loggedIn) {
                $location.path('/login');
            }
        });
    }

})();
