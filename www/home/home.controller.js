(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['UserService','BookingService', '$rootScope', '$location'];
    function HomeController(UserService, BookingService, $rootScope, $location) {
        if($rootScope.globals.currentUser)
            $location.url('/userhome');
        
        var vm = this;
        
        vm.user = null;       
    }

})();