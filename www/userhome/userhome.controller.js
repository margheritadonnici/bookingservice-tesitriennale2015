(function () {
    'use strict';

    angular
        .module('app')
        .controller('UserHomeController', UserHomeController);

    UserHomeController.$inject = ['UserService', 'BookingService', '$rootScope', '$mdDialog'];
    function UserHomeController(UserService, BookingService, $rootScope, $mdDialog) {

        var vm = this;
        vm.user = null;
        vm.showLocationDetails = showLocationDetails;
        vm.showBookingDetails = showBookingDetails;
        vm.error = null;
        vm.errorMessage = null;

        initController();

        function initController() {
            loadCurrentUser();
            vm.dataLoading = true;
            vm.error = false;
            BookingService.GetAllLocationsWithUser(function (response) {
                if (response.isError) {
                    vm.dataLoading = false;
                    vm.error = true;
                    vm.errorMessage = response.errorMessage;
                } else {
                    vm.dataLoading = false;
                    vm.locations = response;
                }
            });
        }

        function loadCurrentUser() {
            vm.user = $rootScope.globals.currentUser;
        }
        
        function showBookingDetails(location) {
            $mdDialog.show({
                controller: ['$scope', 'location', '$mdDialog', function ($scope, location, $mdDialog) {
                    $scope.location = location;
                    $scope.dataLoading = false;
                    $scope.close = function () {
                        $mdDialog.hide();
                    };
                    $scope.error = null;
                    $scope.errorMessage = null;
                    $scope.dataLoading = true;
                    (function initController() {
                        BookingService.GetUpcomingBookings(location.id, function(response) {
                            if (response.isError) {
                                $scope.error = true;
                                $scope.errorMessage = response.errorMessage;
                                $scope.dataLoading = false;
                            } else if (response.noBookingsFound) {
                                $scope.dataLoading = false;
                                $scope.noBookingsFound = true;
                            } else {
                                $scope.dataLoading = false;
                                $scope.events = response.event;
                            }
                        });
                    })();
                }],
                
                
                templateUrl: 'bookingDetails/bookingDetails.tmpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: {
                    location: location,
                },
            });
        }


        function showLocationDetails(location) {
            $mdDialog.show({
                controller: ['$scope', 'location', '$mdDialog', function ($scope, location, $mdDialog) {
                    $scope.location = null;
                    $scope.error = null;
                    $scope.errorMessage = null;
                    $scope.close = function () {
                        $mdDialog.hide();
                    };
                    $scope.dataLoading = true;
                    (function initController() {
                        BookingService.GetLocationDetails(location.id, function (response) {
                            if (response.isError) {
                                $scope.error = true;
                                $scope.errorMessage = response.errorMessage;
                                $scope.dataLoading = false;
                            } else {
                                $scope.dataLoading = false;
                                $scope.location = response.location;
                            }
                        });
                    })();
                }],
                templateUrl: 'locationDetails/locationDetails.tmpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: {
                    location: location,
                },
            });
        };

    }

})();