(function () {
    'use strict';

    angular
        .module('app')
        .controller('AddLocationController', AddLocationController);

    AddLocationController.$inject = ['$location', 'BookingService'];
    function AddLocationController($location, BookingService) {
        var vm = this;
        vm.addLocation = addLocation;
        vm.user = {};
        vm.error = false;

        function addLocation() {
            vm.dataLoading = true;
            vm.error = false;
            var locationRequest = {
                name: vm.name,
                description: vm.description,
                address: vm.address,
                area: vm.area
            };
            BookingService.CreateLocation(locationRequest, function (response) {
                if (response) {
                    console.info(response)
                    if (response.isError) {
                        console.error(response.errorMessage);
                        vm.errorMessage = "Error creating location.";
                        vm.error = true;
                        vm.dataLoading = false;
                        return;
                    }
                    vm.dataLoading = false;
                    $location.path('/userhome');
                } else if (response.isError) {
                    vm.errorMessage = "Error creating location.";
                    vm.error = true;
                    vm.dataLoading = false;
                }
            });
        };


    }

})();
