
// Data types
type Db_Location:void {
	.owner:string
	.id:string
	.name:string
	.description?:string
	.address:string
	.area?:string
}

type Db_LocationNameId:void{
	.name:string
	.id:string
}

type Db_LocationsArray:void{
	.location[0,*]:Db_LocationNameId
}

type Db_User:void {
	.username:string
	.password:string
}

type Db_CreateUserRequest: void {
	.username:string
	.password:string
	.email:string
}

type Db_Booking:void {
	.bookingId?:int
	.calendarId:string
	.eventId:string
	.email:string
	.owner:string
}

type Db_CalendarList:void {
	.calendarIds[0,*]:string
}

// Interface
interface dbConnectorInterface {
RequestResponse:
	getLocationWithId(string)(Db_Location) throws SQLException ConnectionError,
	getAllLocationsWithUser(string)(Db_LocationsArray) throws SQLException ConnectionError,
	insertLocation(Db_Location)(int) throws SQLException ConnectionError,
	deleteLocationWithId(string)(int) throws SQLException ConnectionError,
	verifyPassword(Db_User)(bool) throws SQLException ConnectionError,
	createUser(Db_CreateUserRequest)(int) throws SQLException ConnectionError,
	getBookingInfoWithId(int)(Db_Booking) throws SQLException ConnectionError,
	getBookingInfoWithEvent(string)(Db_Booking) throws SQLException ConnectionError,
	deleteBookingWithId(int)(int) throws SQLException ConnectionError,
	createBooking(Db_Booking)(int) throws SQLException ConnectionError,
	getAllLocations(void)(Db_CalendarList) throws SQLException ConnectionError,
	usernameExists(string)(bool) throws SQLException ConnectionError,
	getOwnerEmail(string)(string) throws SQLException ConnectionError
}