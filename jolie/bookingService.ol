include "dbConnectorInterface.iol"
include "calendarConnectorJolieInterface.iol"
include "bookingServiceInterface.iol"
include "emailNotifierInterface.iol"
include "file.iol"
include "console.iol"
include "converter.iol"
include "string_utils.iol"


execution { concurrent }

outputPort dbService {
  Interfaces: dbConnectorInterface
}

outputPort calendarService {
  Interfaces: CalendarConnectorJolieInterface
}

outputPort emailService {
  Interfaces: EmailNotifierInterface
}

inputPort bookingService {
  Location: "local"
  Interfaces: BookingServiceInterface
}

embedded {
  Jolie: "dbConnector.ol" in dbService
  Jolie: "calendarConnectorJolie.ol" in calendarService
  Jolie: "emailNotifier.ol" in emailService
}

// Procedure to retrieve username and password from headers and store them into the "user" object.
define getUsernameAndPasswordFromHeader {
      base64AuthString = request.authorizationHtmlHeader;

      // Convert the authorization string from base64 to plain.
      base64ToRaw@Converter( base64AuthString )( rawData );
      rawToString@Converter( rawData )( authorizationString );
      
      if (authorizationString == "nil") {
        isLoggedIn = false
      } else {
        authorizationString.regex = ":";
        split@StringUtils( authorizationString )( s );
        user.username = s.result[0];
        user.password = s.result[1]
      }
}


// Install error handlers for "requestScope"
define installErrorHandlers {
      install (
        TypeMismatch =>
          response.isError = true;
          println@Console(requestScope.TypeMismatch)();
          response.errorMessage =  "Type Mismatch Error",
        SQLException =>
          response.isError = true;
          println@Console(requestScope.SQLException.stackTrace)();
          response.errorMessage =  requestScope.SQLException.stackTrace,
        ConnectionError =>
          response.isError = true;
          response.errorMessage =  requestScope.ConnectionError.stackTrace
      )
}

main {

    // Given a booking id, checks its status, returning a string (confirmed, pending or denied)
    [ checkBookingStatus(request)( response ) {
      scope( requestScope ) {
          installErrorHandlers;
          getBookingInfoWithId@dbService(request.bookingID)(bookingResponse);
          calendarRequest.calendarId = bookingResponse.calendarId;
          calendarRequest.eventId = bookingResponse.eventId;
          getBookingStatus@calendarService(calendarRequest)(status);
          if (status == "confirmed") {
            response.status = "confirmed"
          } else if (status == "cancelled") {
            response.status = "denied"
          } else {
            response.status = "pending"
          }
        }
    }]

    // Verifies if password is correct
    [ verifyPassword(request)(response) {
      scope( requestScope ) {
          installErrorHandlers;
          user -> request.user;
          verifyPassword@dbService(user)(response.isVerified)
        }
    } ]

    // Creates a new user in db
    [ createUser( request )( response ) {
        scope( requestScope ) {
            installErrorHandlers;
            // Check if username already exists
            usernameExists@dbService(request.username)(doesUserExist);
            if (doesUserExist) { // if it does, error message
				response.isError = true;
                response.errorMessage = "Username already exists" 
              } else { //otherwise create user and return success
                createUserRequest.username = request.username;
                createUserRequest.password = request.password;
                createUserRequest.email = request.email;
                createUser@dbService(createUserRequest)(_)
            }
        }
    }]

    //Given a time range, returns name and id of all available locations during that time range
    [ getAllAvailableLocations( request )( response ) {
        scope( requestScope ) {
          installErrorHandlers;
          // Get all calendars id from the database in order to pass them as an argument to the calendarConnector function
          getAllLocations@dbService()(allCalendars);
          availableLocationsRequest.calendars -> allCalendars.calendarIds;
          availableLocationsRequest.startDate = request.startDate;
          availableLocationsRequest.endDate = request.endDate;
          //Query to Google Calendar
          getAvailableLocations@calendarService(availableLocationsRequest)(availableCalendars);
          if(#availableCalendars.calendarIds == 0) {
            response.noResultsFound = true
          } else {
			     response.noResultsFound = false;
            for (i=0,i<#availableCalendars.calendarIds, i++) {
                getLocationWithId@dbService(availableCalendars.calendarIds[i])(databaseResult);
                response.location[i].name = databaseResult.name;
                response.location[i].id = databaseResult.id
            }
          }
        }
    }]

    //Given a user, retrieves all locations owned by the user
    [ getAllLocationsWithUser( request )( response ) {
        scope( requestScope ) {
			installErrorHandlers;
			getUsernameAndPasswordFromHeader;
			verifyPassword@dbService(user)(isPasswordVerified); // security check
			if (isPasswordVerified) {
				getAllLocationsWithUser@dbService( user.username )( response )
			}
			
        }
    }]
    
    // Adds a new location in db. Returns true if successfull.
    [ addNewLocation( request )( response ) {
        scope( requestScope ) {
            installErrorHandlers;
            getUsernameAndPasswordFromHeader;
            verifyPassword@dbService(user)(isPasswordVerified);
            if (isPasswordVerified) { //security check
              createCalendar@calendarService(request.name)(calendarId);
              location.owner= user.username;
              location.id = calendarId;
              location.name = request.name;
              if ( is_defined(request.description) ) {
                location.description = request.description
              };
              location.address = request.address;
              if ( is_defined(request.area) ){
                location.area = request.area
              };
              insertLocation@dbService( location )(_)
          }
        }
    }]

    // Create a new booking. Returns booking id.
    [ createBooking( bookingRequest )( response ) {
      scope( requestScope ) {
          installErrorHandlers;

          with(event){
            .calendarId = bookingRequest.calendarId;
            .name = "Event";
            .startDate = bookingRequest.startDate;
            .endDate = bookingRequest.endDate
          };

          //Create corresponding event in google calendar
          createEvent@calendarService(event)(eventId);

          //Retrieve location owner for sending email
          getLocationWithId@dbService(bookingRequest.calendarId)(location);
          locationOwner = location.owner;
          with(booking){
            .calendarId= bookingRequest.calendarId;
            .eventId = eventId;
            .email = bookingRequest.email;
            .owner = locationOwner
          };

          // Save Booking in db
          createBooking@dbService(booking)(bookingId);

          //Retrieve owner email and send notification email
          getOwnerEmail@dbService(locationOwner)(ownerEmail);
          notificationEmail.recipientEmail = ownerEmail;
          notificationEmail.msg = "A Booking has been made for your location. Login to check details.";
          sendNotificationEmail@emailService(notificationEmail)();
          response.bookingId = bookingId
        }
    }]

  //Get all location details from db
  [ getLocationDetails( getLocationDetailsRequest )( response ) {
      scope( requestScope ) {
        installErrorHandlers;
        getLocationWithId@dbService(getLocationDetailsRequest.locationId)(response.location)
      }
    }]

  //Get all upcoming bookings for a location
  [ getUpcomingBookings( getUpcomingBookingsRequest )( response ) {
      scope( requestScope ) {
          installErrorHandlers;
          getAllEvents@calendarService(getUpcomingBookingsRequest.locationId)(eventList);
          if (#eventList.event == 0){
            response.noBookingsFound = true
          } else {
            response -> eventList;
			response.noBookingsFound = false
          }
      }
  }]

  // Deletes a booking
  [deleteBooking(deleteBookingRequest)(response){
      scope( requestScope ) {
          installErrorHandlers;
          if(!(deleteBookingRequest.bookingID instanceof int)) {
			response.isError = true;
            response.errorMessage = "Not a valid ID"
          } else {
            // Retrieve corresponding event id and calendar id to delete event from google calendar
            getBookingInfoWithId@dbService(deleteBookingRequest.bookingID)(databaseResult);
			if (databaseResult.calendarId instanceof string) {
				with (delEventReq) {
				  .calendarId = databaseResult.calendarId;
				  .eventId = databaseResult.eventId
				};
				deleteEvent@calendarService(delEventReq)(_);
        //Delete from database
        deleteBookingWithId@dbService(deleteBookingRequest.bookingID)(_);
				// Send notification email to owner
				getOwnerEmail@dbService(databaseResult.owner)(ownerEmail);
				notificationEmail.recipientEmail = ownerEmail;
				notificationEmail.msg = "A booking for your location has been cancelled. Login for details.";
				sendNotificationEmail@emailService(notificationEmail)()
			} else {
				response.isError = true;
				response.errorMessage = "No booking found for that ID."
			}
          }
      }
  }]
}
