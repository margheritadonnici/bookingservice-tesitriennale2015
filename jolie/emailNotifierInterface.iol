// DataTypes 
type emailRequest:void {
	.recipientEmail:string
	.msg:string
}

// Interface 
interface EmailNotifierInterface {
  	RequestResponse:
  					sendNotificationEmail(emailRequest)(void)
}