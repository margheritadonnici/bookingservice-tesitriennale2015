include "calendarConnectorInterface.iol"
include "calendarConnectorJolieInterface.iol"
include "console.iol"
include "string_utils.iol"

execution { concurrent }

outputPort CalendarConnector {
	Interfaces: CalendarConnectorInterface
}

inputPort CalendarConnectorJolie {
  Location: "local"
  Interfaces: CalendarConnectorJolieInterface
}


embedded {
	Java: 	"org.gradle.CalendarConnector" in CalendarConnector
}

init {
//	Authenticates service account and initializes global calendar instance
	credentials.email = "614373334838-qs8pabv40obje534pk74i519rrp8f7c6@developer.gserviceaccount.com";
	credentials.key = "key/servizioPrenotazioni-32b0fb00864b.p12";
	credentials.appName = "servizio-prenotazioni/1.0";
	serviceAccountInit@CalendarConnector(credentials)();
	println@Console("Authenticated Service Account")()
}

main {
		[createCalendar(name)(response) {
			addCalendar@CalendarConnector(name)(calendarResponse);
			response = calendarResponse
		}]

		[deleteCal(id)(success){
			deleteCalendar@CalendarConnector(id)();
			success = true
		}]

		// Deletes an event
		[deleteEvent(request)(success){
			deleteEvent@CalendarConnector(request)();
			success = true
		}]
		// Checks if a given calendar is busy during a given timespan
		[checkBusy(req)(resp){
			isBusy@CalendarConnector(req)(resp)
		}]
		// Get all events from a calendar (from current date/time)
		[getAllEvents(calendarId)(eventList){
			getEvents@CalendarConnector(calendarId)(eventList)
		}]

		// Given an event data type, creates the event (with given name, start date and end date) and returns it's ID
		[createEvent(request)(response){
			addEvent@CalendarConnector(request)(eventId);
			response = eventId
		}]

		// Set a booking's status as confirmed
		[confirmBooking(request)(success){
			confirmEvent@CalendarConnector(request)();
			success = true
		}]
		// Set a booking's status as cancelled
		[refuseBooking(request)(success){
			cancelEvent@CalendarConnector(request)();
			success = true
		}]
		// Returns a string with booking status (confirmed, tentative or cancelled)
		[getBookingStatus(request)(status){
			getEventStatus@CalendarConnector(request)(status)
		}]

		// Get all "tentative" status events from a calendar (from current date/time)
		[getPendingBookings(calendarId)(eventList){
			getTentativeEvents@CalendarConnector(calendarId)(eventList)
		}]
		// 
		[getAvailableLocations(request)(calendarList){
			getFreeCalendars@CalendarConnector(request)(calendarList)
		}]
}