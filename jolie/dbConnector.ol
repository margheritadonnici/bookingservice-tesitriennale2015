include "database.iol"
include "dbConnectorInterface.iol"
include "console.iol"
include "string_utils.iol"


execution { sequential }

inputPort dbService {
  Location: "local"
  Interfaces: dbConnectorInterface
}

// Install error handlers for scope "requestScope".
define installErrorHandlers {
	install (
		SQLException =>
			println@Console(requestScope.SQLException.stackTrace)();
			throw( SQLException ),
		ConnectionError =>
			println@Console(requestScope.ConnectionError.stackTrace)();
			throw( ConnectionError )
	)
}

init {
	scope ( requestScope ) {
		installErrorHandlers;
		// Establish connection with database
		with (connectionInfo) {
					.host = "127.0.0.1";
					.driver = "mysql";
					.port = 3306;
					.database = "servizioPrenotazioni";
					.username = "root";
					.password = "ciao"
		};
		connect@Database(connectionInfo)();
		println@Console("Connected to database.")()
	}
}



main {

		/************************ Locations ************************/

		// Given an id, returns the corresponding Location
		[getLocationWithId(id)(response){
			scope (requestScope) {
				installErrorHandlers;
				q = "SELECT * FROM spazio WHERE id = :id";
				q.id = id;
				query@Database(q)(databaseResult);
	
				response.owner = databaseResult.row.proprietario;
				response.id = databaseResult.row.id;
				response.name = databaseResult.row.nome;
				response.description = databaseResult.row.descrizione;
				response.address = databaseResult.row.indirizzo;
				response.area = databaseResult.row.quartiere
			}
		}]

		// Given a username, returns an array containing all the Locations owned by the user
		[getAllLocationsWithUser(username)(result){
			scope (requestScope) {
				installErrorHandlers;
				q = "SELECT id, nome FROM spazio WHERE proprietario = :proprietario";
				q.proprietario = username;
				query@Database(q)(databaseResult);
				for (i=0, i<#databaseResult.row, i++){
					result.location[i].name = databaseResult.row[i].nome;
					result.location[i].id = databaseResult.row[i].id
				}
			}
		}]

		// Given a Location, inserts it in the database
		[insertLocation(spazio)(ret){
			scope (requestScope) {
				installErrorHandlers;
				q = "INSERT INTO spazio(proprietario,id,nome,descrizione,indirizzo,quartiere) VALUES (:proprietario,:id,:nome,:descrizione,:indirizzo,:quartiere)";
				q.id = spazio.id;
				q.proprietario = spazio.owner;
				q.nome = spazio.name;
				q.descrizione = spazio.description;
				q.indirizzo = spazio.address;
				q.quartiere = spazio.area;
				update@Database(q)(ret)
			}
		}]

		// Given an id, deletes corresponding Location FROM the database
		[deleteLocationWithId(id)(ret){
			scope (requestScope) {
				installErrorHandlers;
				q = "delete FROM spazio WHERE id = :id";
				q.id = id;
				update@Database(q)(ret)
			}
		}]

		// Returns an array containing all the location ids
		[getAllLocations()(result){
			scope (requestScope) {
				installErrorHandlers;
				q = "SELECT id FROM spazio ";
				query@Database(q)(databaseResult);
				for ( i=0 , i<#databaseResult.row , i++) {
					result.calendarIds[i]=databaseResult.row[i].id
				}
			}

		}]

		/************************ Authentication/Registration ************************/

		//Given username and password, checks if password is correct
		[verifyPassword(request)(ret){
			scope (requestScope) {
				installErrorHandlers;
				q = "SELECT * FROM utente WHERE username = :username";
				q.username = request.username;
				query@Database(q)(databaseResult);
				if (request.password == databaseResult.row.password) {
					ret = true
				} else {
					ret = false
				}
			}
		}]

		[usernameExists(username)(ret){
			scope (requestScope) {
				installErrorHandlers;
				q = "SELECT * FROM utente WHERE username = :username";
				q.username = username;
				query@Database(q)(databaseResult);
				if ( #databaseResult.row == 0) {
					ret = false
				} else {
					ret = true
				}
			}
		}]

		// Adds user in the database
		[createUser(user)(ret){
			scope (requestScope) {
				installErrorHandlers;
				q = "INSERT INTO utente(username,password,email) VALUES (:username,:password,:email)";
				q.username = user.username;
				q.password = user.password;
				q.email = user.email;
				update@Database(q)(ret)
			}
		}]
		
		[getOwnerEmail(owner)(ret){
			scope (requestScope) {
				installErrorHandlers;
				q = "SELECT email FROM utente WHERE username = :username";
				q.username = owner;
				query@Database(q)(databaseResult);
				ret = databaseResult.row.email
			}
		}]

		/************************ Booking ************************/

		[getBookingInfoWithId(id)(ret){
			scope (requestScope) {
				installErrorHandlers;
				q = "SELECT * FROM prenotazione WHERE idprenotazione = :id";
				q.id = id;
				query@Database(q)(databaseResult);
				ret.bookingId = databaseResult.row.idprenotazione;
				ret.calendarId = databaseResult.row.idcalendario;
				ret.eventId = databaseResult.row.idevento;
				ret.email = databaseResult.row.email;
				ret.owner = databaseResult.row.owner
			}
		}]

		[getBookingInfoWithEvent(id)(ret){
			scope (requestScope) {
				installErrorHandlers;
				q = "SELECT * FROM prenotazione WHERE idevento = :id";
				q.id = id;
				query@Database(q)(databaseResult);
				ret.bookingId = databaseResult.row.idprenotazione;
				ret.calendarId = databaseResult.row.idcalendario;
				ret.eventId = databaseResult.row.idevento;
				ret.email = databaseResult.row.email;
				ret.owner = databaseResult.row.owner
			}
		}]

		[deleteBookingWithId(id)(ret){
			scope (requestScope) {
				installErrorHandlers;
				q = "delete FROM prenotazione WHERE idprenotazione = :id";
				q.id = id;
				update@Database(q)(ret)
			}
		}]

		[createBooking(bkg)(ret){
			scope (requestScope) {
				installErrorHandlers;
				q = "INSERT INTO prenotazione(idcalendario,idevento,email,owner) VALUES (:idcal,:idev,:email,:owner)";
				q.idcal = bkg.calendarId;
				q.idev = bkg.eventId;
				q.email = bkg.email;
				q.owner = bkg.owner;
				update@Database(q)(code);
				q = "SELECT idprenotazione FROM prenotazione WHERE idcalendario = :idcal AND idevento = :idev";
				query@Database(q)(id);
				ret = id.row.idprenotazione
			}
		}]


}
