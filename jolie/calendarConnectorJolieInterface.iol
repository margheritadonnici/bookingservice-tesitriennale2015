// Data types
type ServiceAccountCred:void {
	.email:string
	.key:string
	.appName:string
}

type Event:void {
	.calendarId:string
	.eventId[0,1]:string
	.name:string
	.startDate:string 	// format: dd.MM.yyyy,HHmm
	.endDate:string		// format: dd.MM.yyyy,HHmm
}

type BusyReq:void {
	.calendars[1,*]:string
	.startDate:string // dd.MM.yyyy,HHmm
	.endDate:string // dd.MM.yyyy,HHmm
}

type EventList:void {
	.event*: Event
}

type StringReq:void {
	.name:string
}

type StringResp:void {
	.name:string
}

type EvReq:void {
	.calendarId:string
	.eventId:string
}

type CalendarList:void {
	.calendarIds[0,*]:string
}

// Interface 
interface CalendarConnectorJolieInterface {

  	RequestResponse: 	createCalendar(string)(string),
  						deleteCal(string)(bool),
  						createEvent(Event)(string),
  						checkBusy(BusyReq)(bool),
  						getAllEvents(string)(EventList),
  						authenticate(ServiceAccountCred)(bool), 
						deleteEvent(EvReq)(bool),
						confirmBooking(EvReq)(bool),
						refuseBooking(EvReq)(bool),
						getBookingStatus(EvReq)(string),
						getPendingBookings(string)(EventList),
						getAvailableLocations(BusyReq)(CalendarList)
						
}