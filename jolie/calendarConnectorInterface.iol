// Data types
type ServiceAccountCred:void {
	.email:string
	.key:string
	.appName:string
}

type Event:void {
	.calendarId:string
	.eventId[0,1]:string
	.name:string
	.startDate:string 	// format: dd.MM.yyyy,HHmm
	.endDate:string		// format: dd.MM.yyyy,HHmm
}

type BusyReq:void {
	.calendars[1,*]:string
	.startDate:string
	.endDate:string
}

type EventList:void {
	.event*: Event
}

type StringReq:void {
	.name:string
}

type StringResp:void {
	.name:string
}

type EvReq:void {
	.calendarId:string
	.eventId:string
}

type CalendarList:void {
	.calendarIds[0,*]:string
}


// Interface 
interface CalendarConnectorInterface {
  	RequestResponse:
  						addCalendar(string)(string),
  						deleteCalendar(string)(void),
  						addEvent(Event)(string) throws GoogleJsonResponseException,
  						isBusy(BusyReq)(bool),
  						serviceAccountInit(ServiceAccountCred)(void),
  						getEvents(string)(EventList),
  						deleteEvent(EvReq)(void),
						cancelEvent(EvReq)(void),
						confirmEvent(EvReq)(void),
						getEventStatus(EvReq)(string),
						getTentativeEvents(string)(EventList),
						getFreeCalendars(BusyReq)(CalendarList)
}