type User: void {
	.username: string
	.password: string
}


type VerifyPasswordRequest: void {
	.user: User
	.authorizationHtmlHeader: string
}

type CheckBookingStatusRequest: void {
	.bookingID: int
	.authorizationHtmlHeader: string
}

type CreateUserRequest: void {
	.username: string
	.password: string
	.email: string
	.authorizationHtmlHeader: string
}

type GetAllLocationsWithUserRequest: void {
	.authorizationHtmlHeader: string
}

type Location:void {
	.owner:string
	.id:string
	.name:string
	.description?:string
	.address:string
	.area?:string
}

type AddLocationRequest:void {
	.name:string
	.description?:string
	.address:string
	.area?:string
	.authorizationHtmlHeader: string
}

type LocationNameId:void{
	.name:string
	.id:string
}

type GetAllAvailableLocationsRequest:void {
	.startDate:string
	.endDate:string
	.authorizationHtmlHeader: string
}


type CalendarList:void {
	.calendarIds[0,*]:string
}

type CreateBookingRequest:void {
	.calendarId:string
	.startDate:string
	.endDate:string
	.email:string
	.authorizationHtmlHeader:string
}

type GetUpcomingBookingsRequest:void {
	.locationId:string
	.authorizationHtmlHeader:string	
}

type GetLocationDetailsRequest:void {
	.locationId:string
	.authorizationHtmlHeader:string
}

type Event:void {
	.calendarId:string
	.eventId[0,1]:string
	.name:string
	.startDate:string 	
	.endDate:string
}

type DeleteBookingRequest:void{
	.bookingID:int
	.authorizationHtmlHeader:string
}

type CheckBookingStatusResponse:void {
	.status?:string
	.isError?:bool
	.errorMessage?:string
}

type CreateBookingResponse:void {
	.bookingId?:int
	.isError?:bool
	.errorMessage?:string
}

type AddNewLocationResponse:void {
	.isError?:bool
	.errorMessage?:string
}

type VerifyPasswordResponse:void {
	.isVerified?:bool
	.isError?:bool
	.errorMessage?:string
}

type CreateUserResponse:void {
	.isError?:bool
	.errorMessage?:string
}

type DeleteBookingResponse:void {
	.isError?:bool
	.errorMessage?:string
}

type GetAllLocationsWithUserResponse:void {
	.location*:LocationNameId
	.isError?:bool
	.errorMessage?:string
}

type AvailableLocationsResponse:void {
	.location*:LocationNameId
	.isError?:bool
	.errorMessage?:string
	.noResultsFound:bool
}

type GetLocationDetailsResponse:void {
	.location?:Location
	.isError?:bool
	.errorMessage?:string
}

type GetUpcomingBookingsResponse:void {
	.event*: Event
	.noBookingsFound:bool
	.isError?:bool
	.errorMessage?:string
}

interface BookingServiceInterface {
	RequestResponse:
		checkBookingStatus(CheckBookingStatusRequest)(CheckBookingStatusResponse),
		verifyPassword(VerifyPasswordRequest)(VerifyPasswordResponse),
		createUser(CreateUserRequest)(CreateUserResponse),
		getAllLocationsWithUser(GetAllLocationsWithUserRequest)(GetAllLocationsWithUserResponse),
		addNewLocation(AddLocationRequest)(AddNewLocationResponse),
		getAllAvailableLocations(GetAllAvailableLocationsRequest)(AvailableLocationsResponse),
		createBooking(CreateBookingRequest)(CreateBookingResponse),
		getLocationDetails(GetLocationDetailsRequest)(GetLocationDetailsResponse),
		getUpcomingBookings(GetUpcomingBookingsRequest)(GetUpcomingBookingsResponse),
		deleteBooking(DeleteBookingRequest)(DeleteBookingResponse)

}
